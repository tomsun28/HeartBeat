package com.andaily.hb.domain.log.reminder;

import com.andaily.hb.domain.application.ApplicationInstance;
import com.andaily.hb.domain.log.FrequencyMonitorLog;
import com.andaily.hb.domain.log.LogRepository;
import com.andaily.hb.infrastructure.AbstractRepositoryTest;
import com.andaily.hb.infrastructure.DateUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;


/**
 * @author Shengzhao Li
 * @deprecated Use PerMonitoringReminderCheckerTest2.java
 */
@Deprecated
public class PerMonitoringReminderCheckerTest extends AbstractRepositoryTest {


    @Autowired
    private LogRepository logRepository;


    @Test()
    @Ignore
    public void testIsNeedReminder() throws Exception {

        ApplicationInstance applicationInstance = new ApplicationInstance()
                .continueFailedTimes(1)
                .instanceName("Andaily");
        logRepository.saveOrUpdate(applicationInstance);

        FrequencyMonitorLog monitorLog = new FrequencyMonitorLog()
                .instance(applicationInstance).normal(false)
                .costTime(23).createTime(DateUtils.getDate("2014-12-12 12:12:12", DateUtils.DEFAULT_DATE_TIME_FORMAT));
        logRepository.saveOrUpdate(monitorLog);

        fullClean();

        //case 1
        PerMonitoringReminderChecker reminderChecker = new PerMonitoringReminderChecker(monitorLog);
        final boolean needReminder = reminderChecker.isNeedReminder();
        assertTrue(needReminder);


        //case 2
        FrequencyMonitorLog monitorLog2 = new FrequencyMonitorLog()
                .instance(applicationInstance).normal(false)
                .costTime(23).createTime(DateUtils.getDate("2014-12-12 12:12:10", DateUtils.DEFAULT_DATE_TIME_FORMAT));
        logRepository.saveOrUpdate(monitorLog2);

        fullClean();

        reminderChecker = new PerMonitoringReminderChecker(monitorLog);
        assertFalse(reminderChecker.isNeedReminder());

    }
}
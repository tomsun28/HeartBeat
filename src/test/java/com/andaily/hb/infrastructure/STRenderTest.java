package com.andaily.hb.infrastructure;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * 2018/1/31
 *
 * @author Shengzhao Li
 */
public class STRenderTest {


    @Test
    public void test() {

        Map<String, Object> map = new HashMap<>();
        map.put("time", System.currentTimeMillis());
        map.put("url", "http://andaily.com");

        STRender stRender = new STRender("test_render.html", map);
        final String render = stRender.render();

        assertNotNull(render);


    }


}
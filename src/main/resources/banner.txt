${AnsiColor.BRIGHT_GREEN}
  _    _ ____
 | |  | |  _ \
 | |__| | |_) |
 |  __  |  _ <
 | |  | | |_) |
 |_|  |_|____/

HeartBeat: ${application.formatted-version}
Spring Boot: ${spring-boot.formatted-version}

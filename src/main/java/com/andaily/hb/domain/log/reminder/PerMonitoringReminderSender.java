package com.andaily.hb.domain.log.reminder;

import com.andaily.hb.domain.log.FrequencyMonitorLog;
import com.andaily.hb.domain.log.MonitoringReminderLog;

/**
 * @author Shengzhao Li
 */
public interface PerMonitoringReminderSender {


    public boolean support(FrequencyMonitorLog monitorLog);

    public void send(MonitoringReminderLog reminderLog, FrequencyMonitorLog monitorLog);

}